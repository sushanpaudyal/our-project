<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BannerController extends Controller
{
    // Index Page
    public function index(){
        $banners = Banner::all();
        return view ('admin.banner.index', compact('banners'));
    }

    // Add Page
    public function add(){
        return view ('admin.banner.add');
    }

    // Store Banner
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'title' => 'required|max:255',
            'banner_content' => 'required',
            'image' => 'required',
            'link_title' => 'required',
            'link_url' => 'required',
        ];
        $customMessages = [
            'title.required' => 'Banner Title is required',
            'banner_content.required' => 'Banner Content is required',
            'image.required' => 'Banner Image is required',
            'link_title.required' => 'Banner Link Title is required',
            'link_url.required' => 'Banner URL is required',
            'title.max' => 'You are not allowed to enter more than 255 Characters',
        ];
        $this->validate($request, $rules, $customMessages);

        $banner = new Banner();
        $banner->title = $data['title'];
        $banner->banner_content = $data['banner_content'];
        $banner->priority = $data['priority'];
        $banner->link_title = $data['link_title'];
        $banner->link_url = $data['link_url'];

        if (!empty($data['status'])){
            $banner->status = 'active';
        } else {
            $banner->status = 'inactive';
        }

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/banner/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $banner->image = $filename;
            }
        }

        $banner->save();
        Session::flash('success_message', 'Banner has been Added Successfully');
        return redirect()->back();

    }

    // Add Page
    public function edit($id){
        $banner = Banner::findOrFail($id);
        return view ('admin.banner.edit', compact('banner'));
    }

    // Update
    public function update(Request $request, $id){
        $data = $request->all();
        $rules = [
            'title' => 'required|max:255',
            'banner_content' => 'required',
            'link_title' => 'required',
            'link_url' => 'required',
        ];
        $customMessages = [
            'title.required' => 'Banner Title is required',
            'banner_content.required' => 'Banner Content is required',
            'link_title.required' => 'Banner Link Title is required',
            'link_url.required' => 'Banner URL is required',
            'title.max' => 'You are not allowed to enter more than 255 Characters',
        ];
        $this->validate($request, $rules, $customMessages);

        $banner = Banner::findOrFail($id);

        $banner->title = $data['title'];
        $banner->banner_content = $data['banner_content'];
        $banner->priority = $data['priority'];
        $banner->link_title = $data['link_title'];
        $banner->link_url = $data['link_url'];

        if (!empty($data['status'])){
            $banner->status = 'active';
        } else {
            $banner->status = 'inactive';
        }

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/banner/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $banner->image = $filename;
            }
        }

        $banner->save();
        $image_path = 'public/uploads/banner/';
        if(!empty($data['image'])){
            if(file_exists($image_path.$banner->image)){
                unlink($image_path.$data['current_image']);
            }
        }

        Session::flash('success_message', 'Banner has been Updated Successfully');
        return redirect()->route('banner.index');

    }

    public function delete($id){
        $banner = Banner::findOrFail($id);
        $banner->delete();
        $image_path = 'public/uploads/banner/';
        if(file_exists($image_path.$banner->image)){
            unlink($image_path.$banner->image);
        }
        Session::flash('success_message', 'Banner has been Deleted Successfully');
        return redirect()->route('banner.index');
    }
}
