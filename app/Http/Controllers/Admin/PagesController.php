<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AboutUsPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


class PagesController extends Controller
{
    // About Us Page
    public function about(){
        $about = AboutUsPage::first();
        return view ('admin.pages.about', compact('about'));
    }

    // Update About us Page
    public function aboutUpdate(Request $request, $id){
        $about = AboutUsPage::findOrFail($id);
        $data = $request->all();
        $rules = [
            'page_name' => 'required|max:20',
            'page_title' => 'required',
            'page_subtitle' => 'required',
            'page_content' => 'required',
        ];
        $customMessages = [
            'page_name.required' => 'Page Name is required',
            'page_title.required' => 'Page Title is required',
            'page_subtitle.required' => 'Page Sub Title is required',
            'page_content.required' => 'Page Content is required',
            'page_name.max' => 'You are not allowed to enter more than 20 Characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $about->page_name = $data['page_name'];
        $about->page_title = $data['page_title'];
        $about->page_subtitle = $data['page_subtitle'];
        $about->page_content = $data['page_content'];

        $random = Str::random(10);
        if($request->hasFile('image_1')){
            $image_tmp = $request->file('image_1');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $about->image_1 = $filename;
            }
        }

        $random = Str::random(10);
        if($request->hasFile('image_2')){
            $image_tmp = $request->file('image_2');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $about->image_2 = $filename;
            }
        }

        $random = Str::random(10);
        if($request->hasFile('image_3')){
            $image_tmp = $request->file('image_3');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $about->image_3 = $filename;
            }
        }


        $random = Str::random(10);
        if($request->hasFile('image_4')){
            $image_tmp = $request->file('image_4');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $about->image_4 = $filename;
            }
        }

        $about->save();
        Session::flash('success_message', 'About Page has been Updated Successfully');
        return redirect()->back();
    }
}
