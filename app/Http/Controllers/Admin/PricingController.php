<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pricing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PricingController extends Controller
{
    // Index Page
    public function index(){
        $pricings = Pricing::all();
        return view ('admin.price.index', compact('pricings'));
    }

    // Add
    public function add(){
        return view ('admin.price.add');
    }

    // Store
    public function store(Request $request){
        $data = $request->all();




        $rules = [
            'title' => 'required|max:255',
            'features' => 'required',
            'image' => 'required',
            'price' => 'required',
        ];
        $customMessages = [
            'title.required' => 'Pricing Title is required',
            'title.max' => 'You are not allowed to enter more than 255 Characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $pricing = new Pricing();
        $pricing->title = $data['title'];
        $pricing->price = $data['price'];

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/price/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $pricing->image = $filename;
            }
        }

        $features[] = $request->features;
        $featuresAll = json_encode($features);
        $pricing->features = $featuresAll;
        $pricing->save();
        Session::flash('success_message', 'Pricing has been Added Successfully');
        return redirect()->route('pricing.index');
    }


    // Edit Pricing
    public function edit($id){
        $pricing = Pricing::findOrFail($id);
        return view ('admin.price.edit', compact('pricing'));
    }


    // Store
    public function update(Request $request, $id){
        $data = $request->all();
        $rules = [
            'title' => 'required|max:255',
            'features' => 'required',
            'price' => 'required',
        ];
        $customMessages = [
            'title.required' => 'Pricing Title is required',
            'title.max' => 'You are not allowed to enter more than 255 Characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $pricing = Pricing::findOrFail($id);
        $pricing->title = $data['title'];
        $pricing->price = $data['price'];

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/price/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $pricing->image = $filename;
            }
        }

        $features[] = $request->features;
        $featuresAll = json_encode($features);
        $pricing->features = $featuresAll;
        $pricing->save();
        Session::flash('success_message', 'Pricing has been Updated Successfully');
        return redirect()->route('pricing.index');
    }

    public function delete($id){
        $pricing = Pricing::findOrFail($id);
        $pricing->delete();
        Session::flash('success_message', 'Pricing has been Deleted Successfully');
        return redirect()->route('pricing.index');
    }
}
