<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    // Theme Settings
    public function settings(){
        $setting = SiteSetting::first();
        return view ('admin.setting.setting', compact('setting'));
    }

    public function settingsUpdate(Request $request, $id){
        $data = $request->all();
        $setting = SiteSetting::first();
        $setting->email = $data['email'];
        $setting->office_hour = $data['office_hour'];
        $setting->phone = $data['phone'];
        $setting->address = $data['address'];
        $setting->alt_phone = $data['alt_phone'];
        $setting->save();
        Session::flash('success_message', 'Settings has been Updated Successfully');
        return redirect()->back();
    }
}
