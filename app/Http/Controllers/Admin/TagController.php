<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class TagController extends Controller
{
    // Index Page
    public function index(){
        $tags = Tag::orderBy('tag_name', 'ASC')->get();
        return view('admin.cms.tags.index', compact('tags'));
    }

    // Store Category
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'tag_name' => 'required|max:255|unique:tags,tag_name',
        ];
        $customMessages = [
            'tag_name.required' => 'Tag Name is required',
            'tag_name.unique' => 'Tag Name already exists in our database',
        ];
        $this->validate($request, $rules, $customMessages);
        $tag = new Tag();
        $tag->tag_name = strtolower($data['tag_name']);
        $tag->slug = Str::slug($data['tag_name']);
        $tag->save();
        Session::flash('success_message', 'Tag Name has been Added Successfully');
        return redirect()->back();
    }


    // Store Category
    public function update(Request $request, $id){
        $data = $request->all();
        $tag = Tag::findOrFail($id);
        $rules = [
            'tag_name' => 'required|max:255|unique:tags,tag_name,'.$tag->id,
        ];
        $customMessages = [
            'tag_name.required' => 'Tag Name is required',
            'tag_name.unique' => 'Tag Name already exists in our database',
        ];
        $tag->tag_name = $data['tag_name'];
        $tag->slug = Str::slug($data['tag_name']);
        $tag->save();
        Session::flash('success_message', 'Tag Name has been Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $tag = Tag::findOrFail($id);
        $tag->delete();
        Session::flash('success_message', 'Tag has been Deleted Successfully');
        return redirect()->back();
    }
}
