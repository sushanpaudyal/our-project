<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\AboutUsPage;
use App\Models\Banner;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    // Index Page
    public function index(){
        $banners = Banner::orderBy('priority', 'DESC')->get();
        $bannerImage = Banner::orderBy('priority', 'DESC')->first();
        $about = AboutUsPage::first();

        return view ('front.index', compact('banners', 'bannerImage', 'about'));
    }
}
