@extends('admin.includes.admin_design')

@section('content')

    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Banner Management</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Add New Banner</li>
                    </ol>
                </nav>
            </div>
            <div class="ms-auto">
                <a href="{{ route('banner.index') }}" class="btn btn-primary"> <i class="bi bi-eye"></i> View All Banner</a>
            </div>
        </div>
        <!--end breadcrumb-->

        @include('admin.includes._message')

        <div class="row">
            <div class="col-xl-12 mx-auto">

                <div class="card">
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            <h6 class="mb-0 text-uppercase"><span class="text-danger">*</span> ARE REQUIRED FIELDS</h6>
                            <hr/>
                            <form class="row g-3" method="post" action="{{ route('banner.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="col-6">
                                    <label class="form-label" for="title">Banner Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                                </div>

                                <div class="col-3">
                                    <label class="form-label" for="priority">Banner Priority </label>
                                    <input type="number" class="form-control" name="priority" id="priority" value="{{ old('priority') }}">
                                </div>


                                <div class="col-3">
                                    <div class="form-check" style="margin-top: 25px">
                                        <input class="form-check-input" type="checkbox" id="status" name="status" value="1" checked="">
                                        <label class="form-check-label" for="status">
                                            Mark Banner As Active
                                        </label>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="form-label" for="banner_content">Banner Content <span class="text-danger">*</span></label>
                                    <textarea name="banner_content" id="banner_content" cols="30" rows="10" class="form-control">
                                        {{ old('banner_content') }}
                                    </textarea>
                                </div>

                                <div class="col-4">
                                    <label class="form-label" for="image">Banner Image <span class="text-danger">*</span></label>
                                    <input type="file" class="form-control" name="image" id="image" accept="image/*"  onchange="readURL(this)">
                                </div>


                                <div class="col-4">
                                    <label class="form-label" for="link_title">Banner Link Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="link_title" id="link_title">
                                </div>

                                <div class="col-4">
                                    <label class="form-label" for="link_url">Banner Link URL <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="link_url" id="link_url">
                                </div>

                                <div class="col-md-4">
                                    <img src="" alt="" id="one">
                                </div>

                                <div class="col-md-8"></div>


                                <div class="col-2">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-success">Save Information</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end row-->

    </main>
    <!--end page main-->

@endsection

@section('js')

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#banner_content').summernote();
        });
    </script>

    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
