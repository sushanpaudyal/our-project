@extends('admin.includes.admin_design')

@section('content')

    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Banner Management</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Edit Banner</li>
                    </ol>
                </nav>
            </div>
            <div class="ms-auto">
                <a href="{{ route('banner.index') }}" class="btn btn-primary"> <i class="bi bi-eye"></i> View All Banner</a>
            </div>
        </div>
        <!--end breadcrumb-->

        @include('admin.includes._message')

        <div class="row">
            <div class="col-xl-12 mx-auto">

                <div class="card">
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            <h6 class="mb-0 text-uppercase"><span class="text-danger">*</span> ARE REQUIRED FIELDS</h6>
                            <hr/>
                            <form class="row g-3" method="post" action="{{ route('banner.update', $banner->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="col-6">
                                    <label class="form-label" for="title">Banner Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" id="title" value="{{ $banner->title  }}">
                                </div>

                                <div class="col-3">
                                    <label class="form-label" for="priority">Banner Priority </label>
                                    <input type="number" class="form-control" name="priority" id="priority" value="{{ $banner->priority }}">
                                </div>


                                <div class="col-3">
                                    <div class="form-check" style="margin-top: 25px">
                                        <input class="form-check-input" type="checkbox" id="status" name="status" value="1"  {{ $banner->status == 'active'  ? 'checked' : '' }}>
                                        <label class="form-check-label" for="status">
                                            Mark Banner As Active
                                        </label>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="form-label" for="banner_content">Banner Content <span class="text-danger">*</span></label>
                                    <textarea name="banner_content" id="banner_content" cols="30" rows="10" class="form-control">
                                        {{ $banner->banner_content }}
                                    </textarea>
                                </div>

                                <div class="col-4">
                                    <input type="hidden" name="current_image" value="{{ $banner->image }}">
                                    <label class="form-label" for="image">Banner Image <span class="text-danger">*</span></label>
                                    <input type="file" class="form-control" name="image" id="image" accept="image/*"  onchange="readURL(this)">
                                </div>


                                <div class="col-4">
                                    <label class="form-label" for="link_title">Banner Link Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="link_title" id="link_title" value="{{ $banner->link_title }}">
                                </div>

                                <div class="col-4">
                                    <label class="form-label" for="link_url">Banner Link URL <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="link_url" id="link_url" value="{{ $banner->link_url }}">
                                </div>

                                <div class="col-md-4">
                                    <img src="{{ asset('public/uploads/banner/'.$banner->image) }}" alt="" id="one" width="300">
                                </div>

                                <div class="col-md-8"></div>


                                <div class="col-3">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-success">Update Information</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end row-->

    </main>
    <!--end page main-->

@endsection

@section('js')

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#banner_content').summernote();
        });
    </script>

    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>



@endsection
