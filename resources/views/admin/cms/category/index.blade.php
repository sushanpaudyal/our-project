@extends('admin.includes.admin_design')

@section('content')

    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Blog Management</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">View All Categories</li>
                    </ol>
                </nav>
            </div>
            <div class="ms-auto">
                <button data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-primary"> <i class="bi bi-plus"></i> Add New Category</button>
            </div>
        </div>
        <!--end breadcrumb-->
        <hr/>

        @include('admin.includes._message')

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Category Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $category->category_name }}</td>
                                <td>
                                    <a data-bs-toggle="modal" data-bs-target="#exampleModal{{$category->id}}" class="btn btn-sm btn-info" style="color: white">
                                        <i class="bx bx-edit-alt"></i>
                                    </a>
                                    <a href="javascript:" rel="{{ $category->id }}" rel1="delete-category" class="btn btn-sm btn-danger btn-delete" style="color: white">
                                        <i class="bx bx-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>


                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal{{$category->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="row g-3" method="post" action="{{ route('category.update', $category->id) }}">
                                                @csrf
                                                <div class="col-12">
                                                    <label class="form-label" for="category_name">Category Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="category_name" id="category_name" value="{{ $category->category_name }}" >
                                                </div>

                                                <br>

                                                <div class="col-6">
                                                    <div class="d-grid">
                                                        <button type="submit" class="btn btn-success">Update Information</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Category</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form class="row g-3" method="post" action="{{ route('category.store') }}">
                            @csrf
                            <div class="col-12">
                                <label class="form-label" for="category_name">Category Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="category_name" id="category_name" value="{{ old('category_name') }}" >
                            </div>


                            <div class="col-6">
                                <div class="d-grid">
                                    <button type="submit" class="btn btn-success">Save Information</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>





    </main>
    <!--end page main-->



@endsection

@section('js')
    <script>
        $('body').on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var SITEURL = '{{ URL::to('') }}';

            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
        });
    </script>
@endsection
