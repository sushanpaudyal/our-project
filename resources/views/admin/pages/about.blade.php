@extends('admin.includes.admin_design')

@section('content')

    <main class="page-content">

        @include('admin.includes._message')
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <h5 class="mb-0"> About Us Page Details
                        </h5>
                        <hr>
                        <form action="{{ route('aboutUpdate', $about->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card">
                                <div class="card-body">
                                    <div class="border p-4 rounded">
                                        <div class="card-title d-flex align-items-center">
                                            <p class="mb-0 text-right"><span class="text-danger">*</span> ARE REQUIRED FIELDS</p>
                                        </div>
                                        <hr/>
                                        <div class="row mb-3">
                                            <label for="page_name" class="col-sm-3 col-form-label">Page Name <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="page_name" name="page_name" placeholder="Enter Your Website Name" value="{{ $about->page_name }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="page_title" class="col-sm-3 col-form-label">Page Title <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="page_title" name="page_title" placeholder="Enter Your Page Title" value="{{ $about->page_title }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="page_subtitle" class="col-sm-3 col-form-label">Page Sub Title <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="page_subtitle" name="page_subtitle" placeholder="Enter Your Page Title" value="{{ $about->page_subtitle }}">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="page_content" class="col-sm-3 col-form-label">Page Content <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <textarea name="page_content" id="page_content" cols="30" rows="10" class="form-control">
                                                    {{ $about->page_content }}
                                                </textarea>
                                            </div>
                                        </div>


                                        <div class="row mb-3">
                                            <label for="image_1" class="col-sm-3 col-form-label">Image 1</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" id="image_1" name="image_1" placeholder="Enter Your Website Logo" accept="image/*"  onchange="readURL(this)">
                                            </div>
                                        </div>
                                        <br>

                                        <div class="row mb-3">
                                            <label for="image_1" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9">
                                                <img src="{{ asset('public/uploads/'.$about->image_1) }}" alt="" width="120px" id="one">
                                            </div>
                                        </div>


                                        <div class="row mb-3">
                                            <label for="image_1" class="col-sm-3 col-form-label">Image 2</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" id="image_2" name="image_2" placeholder="Enter Your Website Logo" accept="image/*"  onchange="readURL2(this)">
                                            </div>
                                        </div>
                                        <br>

                                        <div class="row mb-3">
                                            <label for="image_2" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9">
                                                <img src="{{ asset('public/uploads/'.$about->image_2) }}" alt="" width="120px" id="two">
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="image_3" class="col-sm-3 col-form-label">Image 3</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" id="image_3" name="image_3" placeholder="Enter Your Website Logo" accept="image/*"  onchange="readURL3(this)">
                                            </div>
                                        </div>
                                        <br>

                                        <div class="row mb-3">
                                            <label for="image_3" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9">
                                                <img src="{{ asset('public/uploads/'.$about->image_3) }}" alt="" width="120px" id="three">
                                            </div>
                                        </div>


                                        <div class="row mb-3">
                                            <label for="image_4" class="col-sm-3 col-form-label">Image 4</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" id="image_4" name="image_4" placeholder="Enter Your Website Logo" accept="image/*"  onchange="readURL4(this)">
                                            </div>
                                        </div>
                                        <br>

                                        <div class="row mb-4">
                                            <label for="image_4" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9">
                                                <img src="{{ asset('public/uploads/'.$about->image_4) }}" alt="" width="120px" id="four">
                                            </div>
                                        </div>


                                        <div class="row">
                                            <label class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn btn-success px-5">Update Settings</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div><!--end row-->

    </main>

@endsection

@section('js')

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#page_content').summernote();
        });
    </script>


    <script type="text/javascript">
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(120);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL2(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#two").attr('src', e.target.result).width(120);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL3(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#three").attr('src', e.target.result).width(120);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL4(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#four").attr('src', e.target.result).width(120);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
