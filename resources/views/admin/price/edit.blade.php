@extends('admin.includes.admin_design')

@section('content')

    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Pricing Management</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Pricing</li>
                    </ol>
                </nav>
            </div>
            <div class="ms-auto">
                <a href="{{ route('pricing.index') }}" class="btn btn-primary"> <i class="bi bi-eye"></i> View All Pricing</a>
            </div>
        </div>
        <!--end breadcrumb-->

        @include('admin.includes._message')

        <div class="row">
            <div class="col-xl-12 mx-auto">

                <div class="card">
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            <h6 class="mb-0 text-uppercase"><span class="text-danger">*</span> ARE REQUIRED FIELDS</h6>
                            <hr/>
                            <form class="row g-3" method="post" action="{{ route('pricing.update', $pricing->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="col-9">
                                    <label class="form-label" for="title">Pricing Title <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="title" id="title" value="{{ $pricing->title }}">
                                </div>

                                <div class="col-3">
                                    <label class="form-label" for="price">Price </label>
                                    <input type="text" class="form-control" name="price" id="price" value="{{ $pricing->price }}">
                                </div>


                                <div class="col-4">
                                    <label class="form-label" for="image">Price Image <span class="text-danger">*</span></label>
                                    <input type="file" class="form-control" name="image" id="image" accept="image/*"  onchange="readURL(this)">
                                </div>

                                <div class="col-8"></div>


                                <div class="col-md-4">
                                    <img src="{{ asset('public/uploads/price/'.$pricing->image) }}" width="300px" alt="" id="one">
                                </div>

                                <div class="col-md-8"></div>


                                <div class="col-6">
                                    <label class="form-label" for="price">Features </label>
                                    @php $resp = json_decode($pricing->features) @endphp
                                    <div class="field_wrapper">
                                        @for($i=0; $i < sizeof($resp[0]); $i++)
                                        <div>
                                            <input type="text" name="features[]" value="{{ $resp[0][$i] }}" class="form-control">
                                            <a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{ asset('public/default/remove-icon.png') }}" style="position: relative; top: -30px; left: 540px;"></a>
                                        </div>
                                        @endfor

                                            <a href="javascript:void(0);" class="add_button" title="Add field"><img src="{{ asset('public/default/add-icon.png') }}" style="position: relative; top: -52px; left: 575px;"></a>

                                    </div>
                                </div>

                                <div class="col-6"></div>


                                <div class="col-2">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-success">Update Information</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end row-->

    </main>
    <!--end page main-->

@endsection

@section('js')

    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = '<div class="field_wrapper">\n' +
                '                                        <div>\n' +
                '                                            <input type="text" name="features[]" class="form-control">\n' +
                '                                            <a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{ asset('public/default/remove-icon.png') }}" style="position: relative; top: -30px; left: 540px;"></a>\n' +
                '                                        </div>\n' +
                '                                    </div>'; //New input field html
            var x = 1; //Initial field counter is 1

            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
            });

            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
    </script>



    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
