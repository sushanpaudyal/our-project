@extends('admin.includes.admin_design')

@section('content')

    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Project Management</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Project</li>
                    </ol>
                </nav>
            </div>
            <div class="ms-auto">
                <a href="{{ route('project.index') }}" class="btn btn-primary"> <i class="bi bi-eye"></i> View All Project</a>
            </div>
        </div>
        <!--end breadcrumb-->

        @include('admin.includes._message')

        <div class="row">
            <div class="col-xl-12 mx-auto">

                <div class="card">
                    <div class="card-body">
                        <div class="border p-3 rounded">
                            <h6 class="mb-0 text-uppercase"><span class="text-danger">*</span> ARE REQUIRED FIELDS</h6>
                            <hr/>
                            <form class="row g-3" method="post" action="{{ route('project.update', $project->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="col-6">
                                    <div class="col-12">
                                        <label class="form-label" for="name">Project Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="name" id="name" value="{{ $project->name }}">
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label" for="image"> Image <span class="text-danger">*</span></label>
                                        <input type="file" class="form-control" name="image" id="image" accept="image/*"  onchange="readURL(this)">
                                    </div>

                                    <div class="col-md-4">
                                        <img src="{{ asset('public/uploads/project/'.$project->image) }}" width="300px;" alt="" id="one">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="col-12">
                                        <label class="form-label" for="details">Project Content <span class="text-danger">*</span></label>
                                        <textarea name="details" id="details" cols="30" rows="10" class="form-control">
                                        {{ $project->details }}
                                    </textarea>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-success">Update Information</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!--end row-->

    </main>
    <!--end page main-->

@endsection

@section('js')

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#details').summernote();
        });
    </script>

    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
