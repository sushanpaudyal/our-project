@extends('admin.includes.admin_design')

@section('content')

    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Project Management</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">View All Projects</li>
                    </ol>
                </nav>
            </div>
            <div class="ms-auto">
                <a href="{{ route('project.add') }}" class="btn btn-primary"> <i class="bi bi-plus"></i> Add New Project</a>
            </div>
        </div>
        <!--end breadcrumb-->
        <hr/>

        @include('admin.includes._message')

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Image</th>
                            <th>Project Name</th>
                            <th>Details</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td><img src="{{ asset('public/uploads/project/'.$project->image) }}" width="200px" alt=""></td>
                                <td>{{ $project->name }}</td>
                                <td style="white-space: normal !important;"> {!!  Str::limit($project->details, 500) !!}</td>
                                <td>
                                    <a href="{{ route('project.edit', $project->id) }}" class="btn btn-sm btn-info" style="color: white">
                                        <i class="bx bx-edit-alt"></i>
                                    </a>
                                    <a href="javascript:" rel="{{ $project->id }}" rel1="delete-project" class="btn btn-sm btn-danger btn-delete" style="color: white">
                                        <i class="bx bx-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </main>
    <!--end page main-->

@endsection

@section('js')
    <script>
        $('body').on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var SITEURL = '{{ URL::to('') }}';

            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
        });
    </script>
@endsection
