@extends('front.includes.front_design')

@section('site_title')
    {{ $blog->blog_title }} - {{ $theme->website_name }} - {{ $theme->website_tagline }}
@endsection

@section('content')

    <!-- start page title area-->
    <div class="page-title-area bg-thin">
        <div class="container">
            <div class="page-title-content">
                <h1>Blog Details</h1>
                <ul>
                    <li class="item"><a href="{{ route('index') }}">Home</a></li>
                    <li class="item"><a href="javascript:">Blog Details</a></li>
                </ul>
            </div>
        </div>
        <div class="shape">
            <span class="shape1"></span>
            <span class="shape2"></span>
            <span class="shape3"></span>
            <span class="shape4"></span>
        </div>
    </div>
    <!-- end page title area -->

    <!-- Start Blog Details section -->
    <section class="blog-details-section ptb-100 bg-thin">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="blog-details-desc">
                        <div class="image">
                            <img src="{{ asset('public/uploads/blog/'.$blog->image) }}" alt="{{ $blog->blog_title }}" />
                        </div>
                        <ul class="post-meta">
                            <li><i class="envy envy-calendar"></i><a href="#">{{ $blog->created_at->format('l, j F Y') }}</a></li>
                            <li><i class="envy envy-comment"></i>03 comments</li>
                        </ul>
                        <div class="content">
                            <h2>{{ $blog->blog_title }}</h2>

                            {!! $blog->blog_content !!}
                        </div>


                        <div class="article-share">
                            <div class="tags pb-3">
                                @php $blog_tags = $blog->tags->sortBy('tag_name')->pluck('id'); @endphp
                                <span>tags:</span>

                                @foreach($blog_tags as $group)
                                    <a href="javascript:">{{ \App\Models\Tag::find($group)->tag_name }}</a>
                                @endforeach



                            </div>

                        </div>
                        <hr />
                        <div class="related-post ptb-30">
                            <h2>related post</h2>
                            <div class="row">
                                @foreach($related_post as $related)
                                <div class="col-md-6 col-sm-12">
                                    <div class="blog-item-single">
                                        <div class="blog-item-img">
                                            <a href="{{ route('blogDetail', $related->slug) }}">
                                                <img src="{{ asset('public/uploads/blog/'.$related->image) }}" alt="{{ $related->blog_title }}" />
                                            </a>
                                            <p class="tag">{{ $related->categories->category_name }}</p>
                                        </div>
                                        <div class="blog-item-content">
                                            <span> <i class="envy envy-calendar"></i>{{ $related->created_at->diffForHumans() }} </span>
                                            <a href="{{ route('blogDetail', $related->slug) }}">
                                                <h3>{{ $related->blog_title }}</h3>
                                            </a>

                                            <a href="{{ route('blogDetail', $related->slug) }}" target="_self" class="btn btn-text-only">
                                                read more
                                                <i class="envy envy-right-arrow"></i>
                                            </a>
                                        </div>
                                        <!-- blog-item-content -->
                                    </div>
                                    <!-- blog-item-single -->
                                </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="comments-area">
                            <h2 class="comment-title">comments</h2>
                            <div id="disqus_thread"></div>
                            <script>
                                /**
                                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
                                /*
                                var disqus_config = function () {
                                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                };
                                */
                                (function() { // DON'T EDIT BELOW THIS LINE
                                    var d = document, s = d.createElement('script');
                                    s.src = 'https://http-localhost-ourproject.disqus.com/embed.js';
                                    s.setAttribute('data-timestamp', +new Date());
                                    (d.head || d.body).appendChild(s);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12">
                    <aside class="widget-area">
                        <section class="widget widget-author">
                            <div class="author-img">
                                @if(!empty($blog->admin->image))
                                <img src="{{ asset('public/uploads/admin/'.$blog->admin->image) }}" alt="{{ $blog->admin->name }} " />
                                @else
                                    <img src="{{ asset('public/default/profile.png') }}" alt="{{ $blog->admin->name }} " />

                                @endif
                            </div>
                            <h5>{{ $blog->admin->name }} </h5>
                        </section>
                        <div class="widget widget-search">
                            <form class="search-form search-top">
                                <input type="search" class="search-field" placeholder="Search article" />
                                <button type="submit" class="btn-text-only">
                                    <i class="envy envy-magnify-glass"></i>
                                </button>
                            </form>
                        </div>
                        <section class="widget widget-article">
                            <h5 class="widget-title">Recent articles</h5>
                            @foreach($recent_articles as $article)
                            <article class="article-item">
                                <a href="{{ route('blogDetail', $article->slug) }}" class="article-img">
                                    <img src="{{ asset('public/uploads/blog/'.$article->image) }}" alt="{{ $article->blog_title }}"  style="width: 111px !important;"/>
                                </a>
                                <div class="info">
                                    <span class="time"><i class="envy envy-calendar"></i>{{ $article->created_at->diffForHumans() }}</span>
                                    <h6 class="title">
                                        <a href="{{ route('blogDetail', $article->slug) }}">{{ $article->blog_title }} </a>
                                    </h6>
                                </div>
                            </article>
                            @endforeach
                        </section>
                        <section class="widget widget-categories">
                            <h5 class="widget-title">Categories</h5>
                            <ul class="categorie-list">
                                @foreach($categories as $category)
                                <li>
                                    <a href="{{ route('categoryBlog', $category->slug) }}">{{ $category->category_name }}</a>
                                    @php $post_count = \App\Models\Blog::where('category_id', $category->id)->count(); @endphp
                                    <span class="total">{{ $post_count }}</span>
                                </li>
                                @endforeach

                            </ul>
                        </section>



                        <section class="widget widget-tag">
                            <h5 class="widget-title">Tags</h5>
                            <div class="tags">
                                @foreach($tags as $tag)
                                <a href="#"> {{ $tag->tag_name }} </a>
                                @endforeach
                            </div>
                        </section>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!-- End Blog Details Area -->

    @endsection
