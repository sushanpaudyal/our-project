@extends('front.includes.front_design')

@section('site_title')
    {{ $theme->website_name }} - {{ $theme->website_tagline }}
@endsection

@section('content')
    <!-- start home banner area -->
    <div id="home" class="home-banner-area banner-type-one" style=" padding-top: 280px; padding-bottom: 200px; background: url({{ asset('public/uploads/banner/'.$bannerImage->image) }}) no-repeat center;">
        <div class="home-slider owl-carousel">
            @foreach($banners as $banner)
            <div class="item">
                <div class="container">
                    <div class="banner-content">
                        <h1>{{ $banner->title }}</h1>
                        <div style="color:white !important; margin-top: 10px;">
                            {!! $banner->banner_content  !!}
                        </div>
                        <div class="cta-btn" style="margin-top: 20px;">
                            <a href="{{ $banner->link_url }}" class="btn btn-solid">{{ $banner->link_title }}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!--end home banner area -->



    <!--start about section-->
    <section id="about" class="about-section ptb-70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-12 pb-30">
                    <div class="section-title">
                        <span class="subtitle">{{ $about->page_title }}</span>
                        <h2>{{ $about->page_subtitle }}</h2>
                    </div>
                    <div class="about-text-blc">
                        {!!  $about->page_content !!}
                    </div>
                    <div class="cta-btn">
                        <a href="{{ route('aboutUs') }}" class="btn btn-solid">
                            Read more
                            <i class="envy envy-right-arrow"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 pb-30">
                    <div class="about-img">
                        <div class="grid-img">
                            <div class="grid-img-inner">
                                <img src="{{ asset('public/uploads/'.$about->image_1) }}" class="image-responsive" alt="office_image" />
                            </div>
                            <div class="grid-img-inner">
                                <img src="{{ asset('public/uploads/'.$about->image_2) }}" class="image-responsive" alt="office_image" />
                            </div>
                            <div class="grid-img-inner">
                                <img src="{{ asset('public/uploads/'.$about->image_3) }}" class="image-responsive" alt="office_image" />
                            </div>
                            <div class="grid-img-inner">
                                <img src="{{ asset('public/uploads/'.$about->image_4) }}" class="image-responsive" alt="office_image" />
                            </div>
                        </div>
                        <div class="logo-overlay">
                            <img src="{{ asset('public/frontend/assets/img/logos/logo.png') }}" alt="logo_without_slogan" />
                        </div>
                        <div class="shape">
                            <img src="{{ asset('public/frontend/assets/img/resource/shape_2.png') }}" alt="shape" class="shape-inner" />
                            <img src="{{ asset('public/frontend/assets/img/resource/shape_4.png') }}" alt="shape" class="shape-inner" />
                            <img src="{{ asset('public/frontend/assets/img/resource/shape_2.png') }}" alt="shape" class="shape-inner" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end about section-->
@endsection
